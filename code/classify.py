import sys, io, random, os, csv, glob
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.tokenize import word_tokenize
from os import listdir
from os.path import isfile, join

#config for unigram, bigram etc.
def get_ngram():
    return 3

#config for train & test with dataset combination
def combined_dataset():
    return False

#include most informative words
def most_informative():
    return True

def get_labels():
    return ["pos", "neg"]

#config for size of dataset
def get_setsize():
    return 4000

#include confusion matrix in output
def confusion_matrix():
    return True


#def limit_percentage():
#    return 0.9


def main():
    if combined_dataset():
        #combime & classify based on percentages in list
        combination_percentage_list = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
        for percentage in combination_percentage_list:
            classify(percentage)
    else:
        classify(0)

def classify(combination_percentage):
    #choose which genre datsets to use
    genre_a = "action"
    genre_b = "simulation"
    labels = get_labels()

    total_a_pos, total_a_neg = load_set(join("data", "csv_source"), genre_a, get_setsize())
    total_a_pos = total_a_pos[0:get_setsize()]
    total_a_neg = total_a_neg[0:get_setsize()]
    total_a = []
    total_a.extend(total_a_pos)
    total_a.extend(total_a_neg)
    
    total_b_pos, total_b_neg = load_set(join("data", "csv_source"), genre_b, get_setsize())
    total_b_pos = total_b_pos[0:get_setsize()]
    total_b_neg = total_b_neg[0:get_setsize()]
    total_b = []
    total_b.extend(total_b_pos)
    total_b.extend(total_b_neg)

    # randomize order/label
    random.shuffle(total_a)
    random.shuffle(total_b)

    #test - train ratio 
    ratio_a = len(total_a)//10
    ratio_b = len(total_b)//10

    if combined_dataset():
        test_labeled_a = total_a[0:ratio_a]
        test_labeled_b = total_b[0:ratio_b]
        labeled_a = total_a[ratio_a:]
        labeled_b = total_b[ratio_b:]
        limit = int(len(labeled_a)//(1/combination_percentage))
        print("limit: ", limit)
        print(len(labeled_a))
        #temp = total_a #copy
        temp = []
        temp.extend(labeled_a)

        labeled_a = labeled_a[limit:]
        labeled_a.extend(labeled_b[0:limit])
        labeled_b = labeled_b[limit:]
        labeled_b.extend(temp[0:limit])
        random.shuffle(labeled_a)
        random.shuffle(labeled_b)

    else:
        test_labeled_a = total_a[0:ratio_a]
        test_labeled_b = total_b[0:ratio_b]
        labeled_a = total_a[ratio_a:]
        labeled_b = total_b[ratio_b:]

    ## Extra print information
    #print("Genre {} loaded, total: {}, positive: {}, negative: {}".format(genre_a, len(total_a), len(total_a_pos), len(total_a_neg)))
    #print("Split into training: {}, testing: {}.".format(len(labeled_a), len(test_labeled_a)))
    #print("Genre {} loaded, total: {}, positive: {}, negative: {}".format(genre_b, len(total_b), len(total_b_pos), len(total_b_neg)))
    #print("Split into training: {}, testing: {}.".format(len(labeled_b), len(test_labeled_b)))

    # Train classifiers
    classifier_a = NaiveBayesClassifier.train(labeled_a)
    if not combined_dataset():
        classifier_b = NaiveBayesClassifier.train(labeled_b)

    # Test and report
    if combined_dataset():
        percentage = combination_percentage*100
        print(percentage)
        #print("Running for {} and {}.".format(genre_a, genre_b))
        print("In domain: trained on {}, testing on {}".format(genre_a, genre_a))
        classifier, confusion, total_count = test_classifier(classifier_a, test_labeled_a)
        print_testresult(classifier, confusion, total_count, combination_percentage)

    else:
        print("In domain: trained on {}, testing on {}".format(genre_a, genre_a))
        test_classifier(classifier_a, test_labeled_a)
        print("In domain: trained on {}, testing on {}".format(genre_b, genre_b))
        test_classifier(classifier_b, test_labeled_b)
       
    if combined_dataset():
        print("Out domain: trained on {}, testing on {}".format(genre_a, genre_b))
        classifier, confusion, total_count = test_classifier(classifier_a, test_labeled_b)
        print_testresult(classifier, confusion, total_count, combination_percentage)  

    else:
        print("Out domain: trained on {}, testing on {}".format(genre_a, genre_b))
        test_classifier(classifier_a, test_labeled_b)
        print("Out domain: trained on {}, testing on {}".format(genre_b, genre_a))
        test_classifier(classifier_b, test_labeled_a)

    if most_informative():
        print("Most informative features for {}".format(genre_a))
        classifier_a.show_most_informative_features(30)
        print("")
        print("Most informative features for {}".format(genre_b))
        classifier_b.show_most_informative_features(30)
    print("-----")
    #print("Done!")

def load_set(p, genre, limit):
    file_directory = join(p, genre)
    print("Loading reviews from: {}".format(file_directory))
    reviews_positive = []
    reviews_negative = []
    for file in glob.glob(os.path.join(file_directory, '*.csv')):
        with open(file, "r", encoding="utf-8") as f:
            reader = csv.reader(f)
            next(reader, None)
            for line in reader:
                if line[6] == "Recommended":
                    reviews_positive.append((get_featuredict(line[5]), "pos"))
                else:
                    reviews_negative.append((get_featuredict(line[5]), "neg"))
                if len(reviews_positive) >= limit and len(reviews_negative) >= limit:
                    return reviews_positive, reviews_negative
    return reviews_positive, reviews_negative

def test_classifier(classifier, test_labeled):
    confusion = {"pos":{"pos":0, "neg":0}, "neg":{"pos":0, "neg":0}}
    for review, expected in test_labeled:
        predicted = classifier.classify(review)
        confusion[expected][predicted] += 1
    if combined_dataset():
        return classifier, confusion, len(test_labeled)
    else:
        print_testresult(classifier, confusion, len(test_labeled), 0)

def print_testresult(classifier, confusion, total_count, percentage):
    ##confusion matrix print
    if confusion_matrix():
        print("Ngram n = {}".format(get_ngram()))
        print("Confusion Matrix")
        print("ex\\ac\tpos\tneg")
        for ex in get_labels():
            print("{}\t{}\t{}".format(ex, confusion[ex]["pos"], confusion[ex]["neg"]))
        print("")
    correct_count = confusion["pos"]["pos"] + confusion["neg"]["neg"]
    print("Correct: {}/{} = {}".format(correct_count, total_count, correct_count/total_count))
    print("")
    if combined_dataset():
        print("({},{})".format(int(percentage*100), correct_count/total_count))

    print("")

def get_featuredict(review):
    return dict([(word, True) for word in ngram_convert(word_tokenize(review, language='english'), get_ngram())])

def ngram_convert(token, n):
    if len(token) < n:
        return [" ".join(token)]
    output = []
    for i in range(len(token)-n+1):
        output.append(" ".join(token[i:i+n]))
    return output


if __name__ == "__main__":
    main()